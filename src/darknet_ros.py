#!/usr/bin/python3
import sys
sys.path.append('/home/skuba/darknet')
import darknet
from queue import Queue
import cv2
from threading import Thread, enumerate
import time
import random
import rospy
from std_msgs.msg import String
from std_srvs.srv import Empty
from sensor_msgs.msg import Image
from cv_bridge import CvBridge, CvBridgeError

class Detection(object):
 
    def __init__(self):
        self.CONFIG_FILE = "/home/skuba/darknet/yolo-obj.cfg"
        self.DATA_FILE = "/home/skuba/darknet/obj.data"
        self.WEIGHTS_FILE = "/home/skuba/darknet/yolo-obj_last.weights"
        self.BATCH_SIZE = 1
        self.INPUT_PATH = 0
        self.THRESHOLD = 0.25
        self.EXT_OUTPUT = True
        self.DISPLAY = True

        self.frame_queue = Queue()
        self.darknet_image_queue = Queue(maxsize=1)
        self.detections_queue = Queue(maxsize=1)
        self.fps_queue = Queue(maxsize=1)

        self.network, self.class_names, self.class_colors = darknet.load_network(
                self.CONFIG_FILE,
                self.DATA_FILE,
                self.WEIGHTS_FILE,
                batch_size=self.BATCH_SIZE
            )
        self.width = darknet.network_width(self.network)
        self.height = darknet.network_height(self.network)
        self.darknet_image = darknet.make_image(self.width, self.height, 3)

        rospy.init_node("object_detection", anonymous=True)
        self.pub = rospy.Publisher("obj_detect", String, queue_size=10)

        self.t1=Thread(target=self.video_capture, args=(self.frame_queue, self.darknet_image_queue))
        self.t2=Thread(target=self.inference, args=(self.darknet_image_queue, self.detections_queue, self.fps_queue))
        self.t3=Thread(target=self.drawing, args=(self.frame_queue, self.detections_queue, self.fps_queue))

        self.terminate_signal = False
        self.started = False

        start_ser = rospy.Service('start_detection', Empty, self.start)
        stop_ser = rospy.Service('stop_detection', Empty, self.stop)

        rospy.Subscriber("/camera/color/image_raw",Image,self.callback)

        self.bridge = CvBridge()

        self.cv_image = None

        rospy.spin()

    def video_capture(self, frame_queue, darknet_image_queue):
        while not rospy.is_shutdown():
            if not self.terminate_signal:
                if self.cv_image is None:
                    break
                frame = self.cv_image
                frame_rgb = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
                frame_resized = cv2.resize(frame_rgb, (self.width, self.height),
                                        interpolation=cv2.INTER_LINEAR)
                frame_queue.put(frame_resized)
                darknet.copy_image_from_bytes(self.darknet_image, frame_resized.tobytes())
                darknet_image_queue.put(self.darknet_image)
        cap.release()

    def callback(self,data):
        try:
            self.cv_image = self.bridge.imgmsg_to_cv2(data, "bgr8")
            cv2.imshow('raw image', self.cv_image)
            cv2.waitKey(1)
        except CvBridgeError as e:
            print(e)


    def inference(self, darknet_image_queue, detections_queue, fps_queue):
        while not rospy.is_shutdown():
            if not self.terminate_signal:
                self.darknet_image = darknet_image_queue.get()
                prev_time = time.time()
                detections = darknet.detect_image(self.network, self.class_names, self.darknet_image, thresh=self.THRESHOLD)
                detections_queue.put(detections)
                fps = int(1/(time.time() - prev_time))
                fps_queue.put(fps)
                print("FPS: {}".format(fps))
                darknet.print_detections(detections, self.EXT_OUTPUT)
                print(detections)
                if(len(detections)>=1):
                    ans = detections[0][0]+","+str(detections[0][2])
                    print(ans)
                else:
                    ans = ""    
                self.pub.publish(ans)
                # darknet.free_image(darknet_image)

    def drawing(self, frame_queue, detections_queue, fps_queue):
        random.seed(3)  # deterministic bbox colors
        while not rospy.is_shutdown():
            if not self.terminate_signal:
                frame_resized = frame_queue.get()
                detections = detections_queue.get()
                fps = fps_queue.get()
                if frame_resized is not None:
                    image = darknet.draw_boxes(detections, frame_resized, self.class_colors)
                    image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
                    if self.DISPLAY:
                        cv2.imshow('Inference', image)
                    if cv2.waitKey(fps) == 27:
                        break
        cv2.destroyAllWindows()

    def start(self, req):   
        if not self.started:
            self.t1.start()
            self.t2.start()
            self.t3.start()
            self.started = True
        else:
            self.terminate_signal = False   

    def stop(self, req):

        self.terminate_signal = True
        cv2.destroyAllWindows() 


if __name__=="__main__":
    detect = Detection()